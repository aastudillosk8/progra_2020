
#asignarle un valor indefinido
greeting_lower = None

#ciclo que se repite hasta que se ingrese un 'hola'
while greeting_lower != 'hola':

    greeting = str(input('vamos... salude: '))
    #permite que haya un mayor parametro para las diferentes maneras de escribir 'hola'
    greeting_lower = greeting.lower()

    #comparar qie el saludo es lo que estamos buscando
    if greeting_lower != 'hola':
        print('no entendi que es: ' + str(greeting))

    pass

print()
print('adios :3')
